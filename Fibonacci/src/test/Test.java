package test;

import static org.junit.Assert.fail;
import search.FibonacciSearch;
import search.InputLessThanZeroException;

public class Test {

	@org.junit.Test
	public void testFibannociSearch() {

		Integer[] testInputs = {1,2,3,4,12,20,29,-1};
		Integer[] testOutpus = {0,1,1,2,89,4181,317811};
		FibonacciSearch fibanocciSearch = new FibonacciSearch();
		for(int i = 0; i < testInputs.length; i++){
			try{
				int resultOfFSearch = fibanocciSearch.search(testInputs[i]);
				if (testOutpus[i] != resultOfFSearch ){
					fail("The input " + testInputs[i].toString() + " has failed.");
				}
			}
			catch(InputLessThanZeroException e){
				//values less than 1 should throw exceptions
			}
			catch(Exception e){
				fail("Unhandled exception");
			}
		}
	}
}

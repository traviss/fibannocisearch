package search;

public class FibonacciSearch {
	
	public int search(int input) throws InputLessThanZeroException{
		if(input < 0){
			InputLessThanZeroException e = new InputLessThanZeroException();
			throw e;
		}
		
	    if(input < 1) {
	        return input;
	    } else {
	        return search(input - 1) + search(input - 2);
	    }
	}
}

﻿using System;
using System.Windows;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TicTacToeSpace;

namespace TicTacToeTest
{
    [TestClass]
    public class TicTacToeTest
    {
        [TestMethod]
        public void test_isWinnerTop()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.TopLeft, null);
            game.InsertPiece(game.MiddleLeft, null);
            game.InsertPiece(game.TopRight, null);
            game.InsertPiece(game.MiddleMiddle, null);
            game.InsertPiece(game.TopMiddle, null);
            
            Assert.AreEqual(game.isWinner(true), true);
        }

        [TestMethod]
        public void test_isWinnerMidRow()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.MiddleRight, null);
            game.InsertPiece(game.TopLeft, null);
            game.InsertPiece(game.MiddleLeft, null);
            game.InsertPiece(game.TopRight, null);
            game.InsertPiece(game.MiddleMiddle, null);
            
            Assert.AreEqual(game.isWinner(true), true);
        }

        [TestMethod]
        public void test_isWinnerBottom()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.BottomLeft, null);
            game.InsertPiece(game.TopLeft, null);
            game.InsertPiece(game.BottomMiddle, null);
            game.InsertPiece(game.TopRight, null);
            game.InsertPiece(game.BottomRight, null);
            
            Assert.AreEqual(game.isWinner(true), true);
        }

        [TestMethod]
        public void test_isWinnerLeft()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.BottomLeft, null);
            game.InsertPiece(game.BottomMiddle, null);
            game.InsertPiece(game.TopLeft, null);            
            game.InsertPiece(game.TopRight, null);
            game.InsertPiece(game.MiddleLeft, null);

            Assert.AreEqual(game.isWinner(true), true);
        }

        [TestMethod]
        public void test_isWinnerMiddleColumn()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.BottomMiddle, null);
            game.InsertPiece(game.BottomLeft, null);
            game.InsertPiece(game.MiddleMiddle, null);
            game.InsertPiece(game.TopLeft, null);
            game.InsertPiece(game.TopMiddle, null);
            

            Assert.AreEqual(game.isWinner(true), true);
        }

        [TestMethod]
        public void test_isWinnerRight()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.TopRight, null);
            game.InsertPiece(game.BottomLeft, null);
            game.InsertPiece(game.MiddleRight, null);
            game.InsertPiece(game.BottomMiddle, null);
            game.InsertPiece(game.BottomRight, null);

            Assert.AreEqual(game.isWinner(true), true);
        }

        [TestMethod]
        public void test_isWinnerLeftDiagonal()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.TopLeft, null);
            game.InsertPiece(game.BottomLeft, null);
            game.InsertPiece(game.MiddleMiddle, null);
            game.InsertPiece(game.BottomMiddle, null);
            game.InsertPiece(game.BottomRight, null);

            Assert.AreEqual(game.isWinner(true), true);
        }

        [TestMethod]
        public void test_isWinnerRightDiagonal()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.TopRight, null);
            game.InsertPiece(game.BottomLeft, null);
            game.InsertPiece(game.MiddleMiddle, null);
            game.InsertPiece(game.BottomMiddle, null);
            game.InsertPiece(game.BottomLeft, null);

            Assert.AreEqual(game.isWinner(true), true);
        }

        [TestMethod]
        public void test_isDraw()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.TopRight, null);
            game.InsertPiece(game.TopMiddle, null);
            game.InsertPiece(game.TopLeft, null);
            game.InsertPiece(game.MiddleLeft, null);
            game.InsertPiece(game.BottomLeft, null);
            game.InsertPiece(game.MiddleMiddle, null);
            game.InsertPiece(game.MiddleRight, null);
            game.InsertPiece(game.BottomRight, null);
            game.InsertPiece(game.BottomMiddle, null);            

            Assert.AreEqual(game.isDraw(true), true);
        }

        [TestMethod]
        public void test_ClearBoard()
        {
            TicTacToe game = new TicTacToe();
            game.InsertPiece(game.TopRight, null);
            game.InsertPiece(game.TopMiddle, null);
            game.InsertPiece(game.TopLeft, null);
            game.InsertPiece(game.MiddleLeft, null);
            game.InsertPiece(game.BottomLeft, null);
            game.InsertPiece(game.MiddleMiddle, null);
            game.InsertPiece(game.MiddleRight, null);
            game.InsertPiece(game.BottomRight, null);
            game.InsertPiece(game.BottomMiddle, null);

            game.clearBoard();

            Assert.IsTrue(String.IsNullOrEmpty(game.TopRight.Text));

            if( game.TopRight.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.TopRight.Name);
            }

            Assert.IsTrue(String.IsNullOrEmpty(game.TopMiddle.Text));

            if (game.TopMiddle.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.TopMiddle.Name);
            }

            Assert.IsTrue(String.IsNullOrEmpty(game.TopLeft.Text));

            if (game.TopLeft.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.TopLeft.Name);
            }

            Assert.IsTrue(String.IsNullOrEmpty(game.MiddleLeft.Text));

            if (game.MiddleLeft.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.MiddleLeft.Name);
            }

            Assert.IsTrue(String.IsNullOrEmpty(game.MiddleMiddle.Text));

            if (game.MiddleMiddle.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.MiddleMiddle.Name);
            }

            Assert.IsTrue(String.IsNullOrEmpty(game.MiddleRight.Text));

            if (game.MiddleRight.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.MiddleRight.Name);
            }

            Assert.IsTrue(String.IsNullOrEmpty(game.BottomRight.Text));

            if (game.BottomRight.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.BottomRight.Name);
            }

            Assert.IsTrue(String.IsNullOrEmpty(game.BottomMiddle.Text));

            if (game.BottomMiddle.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.BottomMiddle.Name);
            }

            Assert.IsTrue(String.IsNullOrEmpty(game.BottomLeft.Text));

            if (game.BottomLeft.Enabled == false)
            {
                Assert.Fail("Clear hasn't disabled this button: " + game.BottomLeft.Name);
            }
        }
    }
}

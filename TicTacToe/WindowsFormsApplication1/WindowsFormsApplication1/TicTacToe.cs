﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToeSpace
{
    public partial class TicTacToe : Form
    {
        public TicTacToe()
        {
            InitializeComponent();
        }

        bool bXTurn = true;
        const String X = "X";
        const String O = "O";

        public void InsertPiece(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;
            clickedButton.Enabled = false;

            if (bXTurn)
            {
                clickedButton.Text = X;
            }
            else
            {
                clickedButton.Text = O;
            }

            if(!isWinner())
                isDraw();

            bXTurn = !bXTurn;
        }

        public bool isWinner(bool isUnitTest = false)
        {
            bool bWinner = false;

            //horizontal
            if (TopLeft.Text == TopMiddle.Text && TopMiddle.Text == TopRight.Text && TopLeft.Enabled == false)
                bWinner = true;
            else if (MiddleLeft.Text == MiddleMiddle.Text && MiddleMiddle.Text == MiddleRight.Text && MiddleLeft.Enabled == false)
                bWinner = true;
            else if (BottomLeft.Text == BottomMiddle.Text && BottomMiddle.Text == BottomRight.Text && BottomLeft.Enabled == false)
                bWinner = true;
            //vertical
            else if (TopLeft.Text == MiddleLeft.Text && MiddleLeft.Text == BottomLeft.Text && TopLeft.Enabled == false)
                bWinner = true;
            else if (TopMiddle.Text == MiddleMiddle.Text && MiddleMiddle.Text == BottomMiddle.Text && TopMiddle.Enabled == false)
                bWinner = true;
            else if (TopRight.Text == MiddleRight.Text && MiddleRight.Text == BottomRight.Text && TopRight.Enabled == false)
                bWinner = true;
            //diagonal
            else if (TopRight.Text == MiddleMiddle.Text && MiddleMiddle.Text == BottomLeft.Text && TopRight.Enabled == false)
                bWinner = true;
            else if (TopLeft.Text == MiddleMiddle.Text && MiddleMiddle.Text == BottomRight.Text && TopLeft.Enabled == false)
                bWinner = true;

            if (!isUnitTest && bWinner)
            {
                String player = "";
                if (bXTurn)
                    player = X;
                else
                    player = O;

                MessageBox.Show("Player " + player + " wins!");
            }
            
            return bWinner;
        }

        public bool isDraw(bool isUnitTest = false)
        {
            bool bisDraw = true;

            foreach (Control control in this.Controls)
            {
                if (control is Button)
                {
                    Button button = control as Button;
                    if(button.Enabled == true)
                    {
                        bisDraw = false;
                        break;
                    }                    
                }
            }

            if (!isUnitTest && bisDraw)
            {
                MessageBox.Show("The game is a draw!");
            }

            return bisDraw;
        }

        public void clearBoard()
        {
            foreach (Control control in this.Controls)
            {
                if (control is Button)
                {
                    Button button = control as Button;
                    button.Text = "";
                    button.Enabled = true;
                }
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clearBoard();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clearBoard();
        }
    }
}

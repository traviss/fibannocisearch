package com.tcompton.prime;

public class PrimeNumberSearch {

	public int search(int input) throws InputLessThanOneException{
		int retVal = 0;
		
		if(input < 1){
			InputLessThanOneException e = new InputLessThanOneException();
			throw e;
		}
		
		int i = 1;
		int j = 1;
		int numOfPrimesFound = -1; //set to -1 because this algorithm thinks 1 is prime
		
		while(i <= 300 ){
			while( j <= i)
			{
				if(j > (i / 2))
				{
					numOfPrimesFound++;
					if(numOfPrimesFound == input){
						return i;
					}
					break;
				}
				else if(i % j == 0)
				{
					break;
				}

				j++;				
			}//end j while
			i++;
			j=2;
		}//i while
		
		return retVal;		
	}
}

package test;

import static org.junit.Assert.fail;

import com.tcompton.prime.InputLessThanOneException;
import com.tcompton.prime.PrimeNumberSearch;

public class Test {

	@org.junit.Test
	public void test() {
		Integer[] testInputs = {1, 7,12,27,41,49,50,-1};
		Integer[] testOutputs = {2, 17,37,103,179,227,229};
		PrimeNumberSearch primeNumberSearch = new PrimeNumberSearch();
		for(int i = 0; i < testInputs.length; i++){
			try{
				int result = primeNumberSearch.search(testInputs[i]);
				if(result != testOutputs[i]){
					fail("The test for: " + testInputs[i] + " fails!" );
				}
			}
			catch(InputLessThanOneException e)
			{
				//this exception is ok!
				//no op
			}
			catch(Exception e){
				fail("Unhandled exception");
			}
		}
	}

}
